#include <iostream>
#include <algorithm>
#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
  _start = start;
  _end = end;
  _on_complementary_dna_strand = on_complementary_dna_strand;
}


unsigned int Gene::get_start() const
{
  return _start;
}

unsigned int Gene::get_end() const
{
  return _end;
}

bool Gene::is_on_complementary_dna_stand() const
{
  return _on_complementary_dna_strand;
}


void Gene::set_start(const unsigned int start)
{
  _start = start;
}

void Gene::set_end(const unsigned int end)
{
  _end = end;
}

void Gene::set_is_on_complementary_dna_strand(const bool _is_on_complementary_dna_strand)
{
  _on_complementary_dna_strand = _is_on_complementary_dna_strand;
}

//#############################################################################################

void Nucleus::init(const std::string dna_sequence)
{
  int i = 0;
  std::string second_DNA_strand = "";

  for (i = 0; i < (dna_sequence.size())-1; i++)
  {
    if (!(dna_sequence[i] == 'A' || dna_sequence[i] == 'G' || dna_sequence[i] == 'C' || dna_sequence[i] == 'T'))
    {

      std::cerr << "Wrong character in gene code!" << '\n';
      _exit(1);
    }
  }
  _DNA_strand = dna_sequence;


  for (i = 0; i < dna_sequence.length(); i++)
  {
    if (dna_sequence[i] == 'G')
    {
      second_DNA_strand += 'C';
    }
    else if (dna_sequence[i] == 'C')
    {
      second_DNA_strand += 'G';
    }
    else if (dna_sequence[i] == 'T')
    {
      second_DNA_strand += 'A';
    }
    else
    {
      second_DNA_strand += 'T';
    }
  }
  _complementary_DNA_strand = second_DNA_strand;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
  int i = 0;
  std::string RNA_transcript = "";

  for (i = gene.get_start(); i <= gene.get_end(); i++)
  {
    if (gene.is_on_complementary_dna_stand())
    {
      RNA_transcript += _complementary_DNA_strand[i];
    }
    else
    {
      RNA_transcript += _DNA_strand[i];
    }
  }

  for (i = 0; i < RNA_transcript.size(); i++)
  {
    if (RNA_transcript[i] == 'T')
    {
      RNA_transcript[i] = 'U';
    }
  }


  return RNA_transcript;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
  std::string reversed_DNA_strand = _DNA_strand;
  reverse(reversed_DNA_strand.begin(), reversed_DNA_strand.end());


  return reversed_DNA_strand;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string temp_codon = codon;

	int result = 0;
	int found = 0;

	while (found != -1)
	{
		found = _DNA_strand.find(temp_codon, found + temp_codon.size());
		if (found != -1)
		{
		  result++;
		}
	}

	return result;
}
