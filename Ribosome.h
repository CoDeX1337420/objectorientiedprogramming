#ifndef RIBOSOME
#define RIBOSOME

#include <iostream>
#include <string>
#include "Protein.h"

class Ribosome
{
  public:
    Protein* create_protein(std::string &RNA_transcript) const;
};

#endif
