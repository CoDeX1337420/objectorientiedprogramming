#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	AminoAcid current_amino_acid;
	std::string current_nukleotid = "";

	protein->init();

	while (RNA_transcript.size() >= 3)
	{
		current_nukleotid = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);
		current_amino_acid = get_amino_acid(current_nukleotid);

		if (current_amino_acid == UNKNOWN)
		{
			protein->clear();
			return nullptr;
		}
		protein->add(current_amino_acid);
	}

	return protein;
}
