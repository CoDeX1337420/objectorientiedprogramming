#include "Mitochondrion.h"

void Mitochondrion::init()
{
  _glucose_level = 0;
  _has_glucose_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
  int i = 0;
  AminoAcidNode* current_node = protein.get_first();

  AminoAcid all_glucose_types[] = {ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END};
  AminoAcid check_glucose_types[7];

  for (i = 0; i < 7; i++)
  {
	  check_glucose_types[i] = current_node->get_data();
	  current_node = current_node->get_next();
  }
  for (i = 0; i < 7; i++)
  {
	  if (all_glucose_types[i] != check_glucose_types[i])
	  {
		  _has_glucose_receptor = false;
		  return;
	  }
  }
  _has_glucose_receptor = true;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
  _glucose_level = _glucose_level + glocuse_units;
}

bool Mitochondrion::produceATP() const
{
  if (_glucose_level >= 50 && _has_glucose_receptor)
  {
    return true;
  }
  return false;
}
