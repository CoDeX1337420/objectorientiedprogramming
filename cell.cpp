#include <iostream>
#include "cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
  _nucleus.init(dna_sequence);
	_glocus_receptor_gene = glucose_receptor_gene;
	_mitochondrion.init();
}

bool Cell::get_ATP()
{
  std::string RNA_transcript = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
  Protein* protein = _ribosome.create_protein(RNA_transcript);

  if (protein == nullptr)
	{
		std::cerr << "Ribosom didn't successfully make a protein..." << std::endl;
		exit(1);
	}

  _mitochondrion.insert_glucose_receptor(*protein);
	_mitochondrion.set_glucose(50);

	protein->clear();
	delete protein;

  if (_mitochondrion.produceATP())
	{
		_atp_units = 100;
		return true;
	}
  else
  {
    return false;
  }
}
