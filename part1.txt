1. reference is more like "another name" to the same variable.
You declare it like a regular variable but you have to add '&' after the type of the variable.

2. A- Avoiding copy of large structures - we can creaet a reference to a large structure instead of coyping it.
   B- It is much safer than pointer - we cant access memory that is not belong to us.

3. In reference we dont save a pointer to a memory where the the variable  is. We just take the variable and 
give it another name.
Reference is safer than pointer because we can take the pointer and increase ITS value,
and by this we can get into memeory we should'nt see it.

4. A- Correct.
   B- Not Correct, because we pass the reference cariable a pointer and that is not correct.
   C- Not Correct, because we have to pass the reference type of variable a VARIABLE and not a NUMBER.

5. A- You return the value of X that is not a reference. It is a STATIC NUMBER.
   B- You make a reference to a memory that allocated dynamically so when you free it, the reference will not be
      deallocated.